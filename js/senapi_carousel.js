(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.senapi_carousel = {
    attach: function(context, settings) {
      if (!context.activeElement) {
          return false;
      }

      var vegas = drupalSettings.vegas || [];
      if (vegas) {
        $('body', context).once('senapi_carousel').vegas(vegas);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
