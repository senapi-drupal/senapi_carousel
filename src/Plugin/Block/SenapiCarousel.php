<?php
namespace Drupal\senapi_carousel\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SenapiCarousel
 * @Block(
 *   id = "senapi_carousel",
 *   admin_label = @Translation("Senapi carousel"),
 *   category = @Translation("Content")
 * )
 *
 * @package Drupal\senapi_carousel\Plugin\Block
 */
class SenapiCarousel extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @inheritdoc
   */
  public function build() {
    $config = $this->getConfiguration();

    switch ($config['carousel_style']) {
      case 'vegas':
        $slides = [];
        foreach ($this->getSlides($config) as $slide) {
          if (isset($slide['image'])){
            array_push($slides, ['src' => file_create_url($slide['image'])]);
          }
        }

        $settings['slides'] = $slides;
        $settings['delay'] = 35000;
        $settings['overlay'] = 1;
        $settings['timer'] = FALSE;
        $settings['shuffle'] = FALSE;

        $settings['firstTransition'] = 'fade2';

        $settings['transition'] = ['fade2'];
        $settings['slidesToKeep'] = 1;
        #$settings['animation'] = "random";

        return [
          '#type' => 'container',
          '#attached' => [
            'library' => ['senapi_carousel/vegas'],
            'drupalSettings' => [
              'vegas' => $settings,
            ]
          ]
        ];
        break;
      default:
        return [
        '#theme' => 'block__senapi_carousel',
        'slides' => $this->getSlides($config),
        'id' => Html::getUniqueId('senapi_carousel'),
        'config' => $config
      ];
        break;
    }

  }

  /**
   * @inheritdoc
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['user.node_grants:view', 'languages:language_interface']
    );
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(),
      ['node_list', 'config:block.block.senapi_carousel']
    );
  }

  /**
   * @inheritdoc
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
      'entity_selected' => 'node',
      'content_types' => [],
      'publishing_options' => ['status' => 1],
      'skip_content_without_image' => FALSE,
      'image' => 'field_slide_image',
      'title' => 'title',
      'image_style' => '',
      'url' => '',
      'description' => 'field_slide_text',
      'description_truncate' => 300,
      'order_field' => 'created',
      'order_direction' => 'DESC',
      'limit' => 5,
      'filter_by_field' => '',
      'filter_by_field_operator' => '=',
      'filter_by_field_value' => '',
      'carousel_style' => 'controls',
      'more_link' => '',
      'more_link_text' => 'See more',
      'data_interval' => 5000
    ];
  }

  /**
   * @inheritdoc
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $defaults = $this->defaultConfiguration();

    if ($form_state instanceof SubformStateInterface) {
      $ajax_values = $form_state->getCompleteFormState()->getValues();
    } else {
      $ajax_values = $form_state->getValues();
    }

    if (isset($ajax_values['settings']['senapi_carousel_settings']['content_selection']['entity_selected'])) {
      $config['entity_selected'] = $ajax_values['settings']['senapi_carousel_settings']['content_selection']['entity_selected'];
    }

    if (empty($config['entity_selected'])) {
      $config['entity_selected'] = $defaults['entity_selected'];
    }

    if (empty($config['image']) && empty($config['title']) && empty($config['description'])) {
      $config['title'] = $defaults['title'];
    }

    $form['senapi_carousel_settings'] = [
      '#type' =>  'fieldset',
      '#title' => 'Senapi carousel configuration',
      '#descripton' => 'Configure the content selection and the slide fields used in senapi carousel.',
      '#attributes' => ['id' => 'senapi-carousel-wrapper'],
    ];

    $form['senapi_carousel_settings']['content_selection'] = [
      '#type' => 'details',
      '#title' => 'Content selection and ordering',
      '#description' => 'Filter, limit and sort the contents shwn on slides.',
      '#open' => TRUE,
      '#attributes' => ['id' => 'content-selection-wrapper']
    ];

    $form['senapi_carousel_settings']['slide_fields'] = [
      '#type' => 'details',
      '#title' => 'Slide fileds and styling',
      '#description' => 'Assign the fields used as image, title, description an link on slides.',
      '#open' => TRUE,
      '#attributes' => ['id' => 'slide-fields-wrapper']
    ];

    $form['senapi_carousel_settings']['content_selection']['entity_selected'] = [
      '#type' => 'select',
      '#title' => 'Entity type',
      '#default_value' => $config['entity_selected'],
      '#options' => $this->getEntityTypes(),
      '#description' => 'Check the entity that you want to use in the carousel. Default: node',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxFormSettingsCallback'],
        'wrapper' => 'senapi-carousel-wrapper',
        'event' => 'change'
      ]
    ];

    $bundles = $this->getEntityTypeBundles($config['entity_selected']);
    if (!empty($bundles)) {
      $form['senapi_carousel_settings']['content_selection']['content_types'] = [
        '#type' => 'checkboxes',
        '#title' => 'Entity type bundles',
        '#default_value' => !empty($config['content_types'])? $config['content_types']  : [],
        '#options' => $bundles,
        '#description' => '',
        '#prefix' => '<div class="form-type-radios">',
        '#sufix' => '</div>',
        '#attributes' => ['class' => ['container-inline']],
        '#validated' => TRUE,
      ];
    }

    $options = [
      'status' => 'Published',
      'promote' => 'Promoted to front page',
      'sticky' => 'Sticky at top of lists'
    ];

    if ($config['entity_selected'] === 'node') {
      if ($this->moduleHandler->moduleExists('custom_pub')) {
        $publish_types = $this->entityTypeManager
            ->getStorage('custom_publishing_option')
            ->loadMultiples();

        foreach ($publish_types as $publish_type) {
          $options[$publish_type->id()] = $publish_type->label();
        }
      }
    }

    $form['senapi_carousel_settings']['content_selection']['publishing_options'] = [
      '#type' => 'checkboxes',
      '#title' => 'Publishing options',
      '#default_value' => !empty($config['publishing_options']) ? $config['publishing_options'] : [],
      '#options' => $options,
      '#description' => 'Publishing options to filter content in the carousel.',
      '#validated' => TRUE,
    ];

    $form['senapi_carousel_settings']['content_selection']['skip_content_without_image'] = [
      '#type' => 'checkbox',
      '#title' => 'Skip content without image',
      '#default_value' => $config['skip_content_without_image'],
      '#description' => 'Ensure that all carousel content has image.'
    ];

    $form['senapi_carousel_settings']['content_selection']['filter_by_field'] = [
      '#type' => 'select',
      '#title' => 'Filter by field',
      '#options' => $this->getFields($config['entity_selected'], FALSE),
      '#default_value' => $config['filter_by_field'],
      '#description' => 'Select  the field you want to use as filter.',
      '#empty_option' => '- None -'
    ];

    $form['senapi_carousel_settings']['content_selection']['filter_by_field_operator'] = [
      '#type' => 'select',
      '#title' => 'Filter by field operator',
      '#options' => [
        '=' => 'Equal',
        '<>' => 'Not equal',
        'CONTAINS' => 'Contains',
        '>' => 'Greater than',
        '>=' => 'Greater  than or equal',
        '<' => 'Less than',
        '<=' => 'Less than or equal'
      ],
      '#default_value' => $config['filter_by_field_operator'],
      '#description' => 'Select the comparasion operator to use in filter field.',
      '#states' => [
        'visible' => [
          ':input[name="settings[senapi_carousel_settings][content_selection][filter_by_field]"]' => ['!value' => '']
        ]
      ]
    ];

    $form['senapi_carousel_settings']['content_selection']['filter_by_field_value'] = [
      '#type' => 'textfield',
      '#title' => 'Filter by field value',
      '#default_value' => $config['filter_by_field_value'],
      '#description' => 'Select the value you want to use as field filter. If you filter by field that contains taxonomy terms or relations content you should us the tid or entity id as value.',
      '#state' => [
        'visible' => [
          ':input[name="settings[senapi_carousel_settings][content_selection][filter_by_field]"]' => ['!value' => '']
        ]
      ]
    ];

    $option_types = ['integer', 'created', 'changed', 'datetime'];
    $form['senapi_carousel_settings']['content_selection']['order_field'] = [
      '#type' => 'select',
      '#title' => 'Order by',
      '#options' => $this->getFieldByType($option_types, $config['entity_selected']),
      '#default_value' => $config['order_field'],
      '#empty_option' => '- None -',
      '#validated' => TRUE
    ];


    $form['senapi_carousel_settings']['content_selection']['order_direction'] = [
      '#type' => 'select',
      '#title' => 'Order direction',
      '#options' => [
        'ASC' => 'Ascending',
        'DESC' => 'Descending',
        'RANDOM' => 'Random',
      ],
      '#default_value' => $config['order_direction'],
      '#states' => [
        'visible' => [
          ':input[name="settings[senapi_carousel_settings][content_selection][order_by]"]' => ['!value' => '']
        ]
      ]
    ];

    $form['senapi_carousel_settings']['content_selection']['limit'] = [
      '#type' => 'number',
      '#title' => 'Max number of elements',
      '#default_value' => $config['limit'],
      '#description' => 'The maximum number of elements to show in the carousel.'
    ];

    $form['senapi_carousel_settings']['slide_fields']['carousel_style'] = [
      '#type' => 'select',
      '#title' => 'Carousel layout style',
      '#options' => [
        'default' => 'Bootstrap default',
        'without-indicators' => 'Bootstrap without indicators',
        'controls' => 'Bootstrap with controls',
        'vegas' => 'Vega Background',
      ],
      '#default_value' => $config['carousel_style'],
      '#description' => 'The carousel style controls of visualization and carousel templating',
      '#required' => TRUE,
    ];

    $form['senapi_carousel_settings']['slide_fields']['image'] = [
      '#type' => 'select',
      '#title' => 'Image field',
      '#options' => $this->getFieldByType(['image'], $config['entity_selected']),
      '#default_value' => $config['image'],
      '#empty_option' => '- None -',
      '#validated' => TRUE,
    ];

    $form['senapi_carousel_settings']['slide_fields']['image_style'] = [
      '#type' => 'select',
      '#title' => 'Image style',
      '#options' => $this->getImageStyles(),
      '#default_value' => $config['image_style'],
      '#empty_option' => '- None -',
      '#validated' => TRUE,
    ];

    $form['senapi_carousel_settings']['slide_fields']['title'] = [
      '#type' => 'select',
      '#title' => 'Title field',
      '#options' => $this->getFieldByType(['string'], $config['entity_selected']),
      '#default_value' => $config['title'],
      '#empty_option' => '- None -'
    ];

    $url_options = ['canonical' => 'Link to entity content'];
    $url_options = array_merge($url_options, $this->getFieldByType(['file', 'link'], $config['entity_selected']));

    $form['senapi_carousel_settings']['slide_fields']['url'] = [
      '#type' => 'select',
      '#title' => 'Link field',
      '#options' => $url_options,
      '#default_value' => explode('|', $config['url']),
      '#emtpy_option' => '- None -',
      '#validated' => TRUE,
      '#multiple' => TRUE,
    ];

    $option_types = ['text_with_summary', 'text_long', 'string'];
    $form['senapi_carousel_settings']['slide_fields']['description'] = [
      '#type' => 'select',
      '#title' => 'Description field',
      '#options' => $this->getFieldByType($option_types, $config['entity_selected']),
      '#default_value' => $config['description'],
      '#empty_option' => '- None -',
      '#validated' => TRUE,
    ];

    $form['senapi_carousel_settings']['slide_fields']['description_truncate'] = [
      '#type' => 'number',
      '#title' => 'Maximun number of charachers in description field',
      '#default_value' => $config['description_truncate'],
      '#description' => 'Truncates the description to a maximum number of characters. Truncation attempts to truncate on a word boundary. Use 0 for unlimited',
      '#states' => [
        'visible' => [
          ':input[name="settings[senapi_carousel_settings][slide_fields][description]"]' => ['!value' => '']
        ]
      ]
    ];

    $form['senapi_carousel_settings']['slide_fields']['more_link'] = [
      '#type' => 'url',
      '#title' => 'More link',
      '#default_value' => $config['more_link'],
      '#description' => 'This will add a more link to the bottom of the carousel.',
    ];

    $form['senapi_carousel_settings']['slide_fields']['more_link_text'] = [
      '#type' => 'textfield',
      '#title' => 'More link text',
      '#default_value' => $config['more_link_text'],
      '#states' => [
        'visible' => [
          ':input[name="settings[senapi_carousel_settings][slide_fields][more_link]"]' => ['filled' => TRUE]
        ]
      ]
    ];

    $form['senapi_carousel_settings']['slide_fields']['data_interval'] = [
      '#type' => 'number',
      '#title' => 'Slidese interval',
      '#default_value' => (int) $config['data_interval'],
      '#min' => 0,
      '#description' => 'The amount of time (in ms) to delay between automatically cycling an item. If 0, carousel will not automatically cycle.'
    ];

    return $form;
  }

  public function ajaxFormSettingsCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
    return $form['settings']['senapi_carousel_settings'];
  }

  /**
   * @inheritdoc
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
   $config = $form_state->getValues();
   if (isset($config['senapi_carousel_settings'])) {
     $content_selection = [
       'entity_selected',
       'content_types',
       'publishing_options',
       'skip_content_without_image',
       'order_field',
       'order_direction',
       'limit',
       'filter_by_field',
       'filter_by_field_operator',
       'filter_by_field_value'
     ];

     foreach ($content_selection as $config_field) {
       if (isset($config['senapi_carousel_settings']['content_selection'][$config_field])) {
         $this->setConfigurationValue(
           $config_field,
           $config['senapi_carousel_settings']['content_selection'][$config_field]
         );
       }
     }

     $slide_fields = [
       'carousel_style',
       'image',
       'image_style',
       'title',
       'url',
       'description',
       'description_truncate',
       'more_link',
       'more_link_text',
       'data_interval'
     ];

     foreach ($slide_fields as $config_field) {
       if (isset($config['senapi_carousel_settings']['slide_fields'][$config_field])) {
         $this->setConfigurationValue(
           $config_field,
           ($config_field == 'url') ? join('|', array_keys($config['senapi_carousel_settings']['slide_fields'][$config_field])) : $config['senapi_carousel_settings']['slide_fields'][$config_field]
         );
       }
     }

     Cache::invalidateTags(['config:block.block.senapi_carousel']);
   }
  }

  private function getEntityTypes() {
    $entities = [];
    $entity_definitions = $this->entityTypeManager->getDefinitions();
    if (!empty($entity_definitions)) {
      foreach ($entity_definitions as $entity_definition) {
        if ($entity_definition instanceof ContentEntityType && $entity_definition->get('field_ui_base_route')) {
          $entities[$entity_definition->id()] = $entity_definition->id();
        }
      }
    }

    return $entities;
  }

  private function getFields($entity_type = 'node', $grouped = TRUE) {
    $fields = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
    $options = [];

    foreach ($fields as $field) {
      if ($grouped) {
        $options[$field->getType()][$field->getName()] = $field->getLabel() . ' (' . $field->getName() . ')';
      }
      else {
        $options[$field->getName()] = $field->getLabel() . ' (' . $field->getName() . ')';
      }
    }

    return $options;
  }

  private function getImageStyles() {
    $styles = $this->entityTypeManager->getStorage('image_style')->loadMultiple();
    $options = [];
    foreach ($styles as $key => $style) {
      $options[$key] = $key;
    }

    return $options;
  }

  private function getFieldByType($types, $entity = 'node') {
    $fields = $this->getFields($entity);

    $options = [];
    foreach ($types as $type) {
      if (isset($fields[$type])) {
        $options = array_merge($options, $fields[$type]);
      }
    }

    return $options;
  }

  private function getEntityTypeBundles($entity) {
    $options = [];

    $entity_type = $this->entityTypeManager->getDefinition($entity)->getBundleEntityType();
    if (!empty($entity_type)) {
      $entity_type_bundles = $this->entityTypeManager->getStorage($entity_type)->loadMultiple();
      if (!empty($entity_type_bundles)) {
        foreach ($entity_type_bundles as $entity_type_bundle) {
          $options[$entity_type_bundle->id()] = $entity_type_bundle->label();
        }
      }
    }

    return $options;
  }

  private function getQueriedEntities($config) {
    $entities = [];
    $entity_type = !empty($config['entity_selected']) ? $config['entity_selected']  : 'node';
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $query = $storage->getQuery();

    if (!empty($config['content_types'])) {
      $bundles = $this->getValidBundles($config['content_types'], $config['entity_selected']);
      $entity_keys = $this->entityTypeManager->getDefinition($entity_type)->get('entity_keys');
      if (!empty($bundles) && isset($entity_keys['bundle'])) {
        $query->condition($entity_keys['bundle'], array_values($bundles), 'IN');
      }
    }

    if (isset($config['skip_content_without_image']) && $config['skip_content_without_image'] === TRUE) {
      if (!empty($config['image'])) {
        $query->condition($config['image'], NULL, 'IS NOT NULL');
      }
    }

    if (!empty($config['publishing_options'])) {
      foreach ($config['publishing_options'] as $key => $value) {
        if (!empty($value)) {
          $query->condition($key, 1);
        }
      }
    }

    if (!empty($config['filter_by_field']) && isset($config['filter_by_field_value'])) {
      $operator = !empty($config['filter_by_field_operator']) ? $config['filter_by_field_operator'] : '=';
      $query->condition($config['filter_by_field'], $config['filter_by_field_value'], $operator);
    }

    if (isset($config['order_direction']) && $config['order_direction'] == 'RANDOM') {
      $query->addTag('random_order');
    }
    elseif (!empty($config['order_field'])) {
     if ($config['order_direction'] == 'ASC' || $config['order_direction'] == 'DESC') {
       $query->sort($config['order_field'], $config['order_direction']);
     }
     else {
       $query->sort($config['order_field']);
     }
    }

    if (!empty($config['limit'])) {
      $query->range(0, $config['limit']);
    }

    $entity_ids = $query->execute();

    if (!empty($entity_ids)) {
      $entities = $storage->loadMultiple($entity_ids);
    }

    return $entities;
  }

  private function getValidBundles($bundles, $entity) {
    $entityBundles = $this->getEntityTypeBundles($entity);
    $valid_bundles = [];
    foreach ($bundles as $key => $value) {
      if (!empty($value) && isset($entityBundles[$key])) {
        $valid_bundles[] = $key;
      }
    }

    return $valid_bundles;
  }

  private function composeSlide($config, $entity) {
    $title = '';
    if (!empty($config['title'])) {
      $title = strip_tags($entity->{$config['title']}->value);
    }

    $description = '';
    if (!empty($config['description']) && isset($entity->{$config['description']})) {
      $description = trim(strip_tags($entity->{$config['description']}->value));
      if (!empty($config['description_truncate']) && $config['description_truncate'] > 0) {
        $description = Unicode::truncate($description, $config['description_truncate'], TRUE, TRUE);
      }
    }

    $url = '';
    $url_external = false;
    $urls = explode('|', $config['url']);

    foreach ($urls as $_url) {
      if ($_url === 'canonical' || $config['url'] === 'nid') {
        $url = $entity->toUrl('canonical');
        break;
      }
      elseif(!empty($_url) && isset($entity->{$_url}) && !$entity->{$_url}->isEmpty()) {
        if ($entity->{$_url}->first()->target_id) {
          $url = file_create_url(File::load($entity->{$_url}->first()->target_id)->getFileUri());
          break;
        }
        else {
          if ($entity->{$_url}->first()->getUrl()->isExternal()) {
            $url = $entity->{$_url}->first()->getUrl()->getUri();
            $url_external = true;
            break;
          }
          elseif($entity->{$_url}->first()->getUrl()->isRouted()) {
            $url = file_create_url($entity->{$_url}->first()->getUrl()->getInternalPath());
            break;
          }
        }
      }
    }

    $image_width = $image_height = $image_uri = '';
    if (!empty($config['image']) && isset($entity->{$config['image']})) {
      $image_obj = $entity->{$config['image']}->entity;
      if (!empty($image_obj)) {
        $image_uri = $image_obj->getFileUri();
      }
      else {
        $default_image = $entity->{$config['image']}->getSetting('default_image');
        if (!empty($default_image) && isset($default_image['uuid'])) {
          $default_entity = $this->entityRepository->loadEntityByUuid('file', $default_image['uuid']);
          if (!empty($default_entity)) {
            $image_uri = $default_entity->getFileUri();
          }
        }
      }

      if (!empty($image_uri)) {
        if (!empty($config['image_style'])) {
          $style = ImageStyle::load($config['image_style']);
          $image_derivative = $style->buildUri($image_uri);

          if (!file_exists($image_derivative)) {
            $style->createDerivative($image_uri, $image_derivative);
          }
          $image_uri = $image_derivative;
        }
      }

      $image = $this->imageFactory->get($image_uri);
      if ($image->isValid()) {
        $image_width = $image->getWidth();
        $image_height = $image->getHeight();
      }
    }

    return [
      'image' => $image_uri,
      'image_width' => $image_width,
      'image_height' => $image_height,
      'title' => $title,
      'url' => $url,
      'url_external' => $url_external,
      'description' => $description
    ];
  }

  private function getSlides($config) {
    $slides = [];

    $entities = $this->getQueriedEntities($config);
    if (!empty($entities)) {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
      foreach ($entities as $entity) {
        if ($entity->hasTranslation($langcode)) {
          $entity = $entity->getTranslation($langcode);
        }
        $slides[] = $this->composeSlide($config, $entity);
      }
    }

    return $slides;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('image.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('language_manager')
    );
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, EntityFieldManagerInterface $entity_field_manager, ImageFactory $image_factory, EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->entityFieldManager = $entity_field_manager;
    $this->imageFactory = $image_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
  }
}